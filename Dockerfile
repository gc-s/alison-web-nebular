FROM node:14

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install && npm -i -g @angular/cli

COPY . .

EXPOSE 4200