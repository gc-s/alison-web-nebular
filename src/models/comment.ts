import { Member } from "./member";

export type Comment = {
    id: number;
    idUser: Member;
    comment: string
    fecha: Date;
    propietario: boolean;
}