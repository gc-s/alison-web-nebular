import { Rol } from "./rol";

export type Member = {
    id: number;
    nombre: string;
    email: string;
    usuario: string;
    rol?: Rol;
    propietario: boolean;
    assigned?: boolean;
}