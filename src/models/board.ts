import { Member } from './member';
import { Rol } from './rol';

export type Board = {
	id: number;
	clave: string;
	nombre: string;
	fondo: string;
	miembros?: Member[];
	rol?: Rol;
	propietario: boolean;
};
