import { Task } from "./task";

export type List = {
    id: number;
    nombre: string;
    wip: number;
    tareas: Task[];
    posicion: number;
    archivo: boolean;
}