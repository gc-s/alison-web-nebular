import { Label } from "./label";
import { Member } from './member';

export type Task = {
    id: number;
    titulo: string;
    descripcion: string;
    etiquetas: Label[];
    miembros: Member[];
    fechaInicio: Date;
    fechaLimite: Date;
    fechaCreacion: Date;
    fechaTermino: Date;
    tamañoEstimado: number;
    tamañoReal: number;
    archivo: boolean;
}