export type ApiResponse = {
    statusCode: number;
    message: string;
    error: string;
}