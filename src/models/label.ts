export type Label = {
    id: number;
    nombre: string;
    color: string;
    check?: boolean;
}