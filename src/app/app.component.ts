import { Component } from '@angular/core';
import { AlisonService } from './services/alison.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'alison-web';
  loading: boolean = true;

  constructor(private api: AlisonService) {
    this.api.auth.getApiStatus().subscribe( () => {
      this.loading = false;
    }, (error) => {
      console.log(error);
    })
  }
  
  

}
