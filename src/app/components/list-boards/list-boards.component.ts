import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { DashboardModel } from 'src/app/models/dashboard/dashboard.model';
import { AlisonService } from 'src/app/services/alison.service';
import { DialogBoardComponent } from '../modals/dialog-board/dialog-board.component';
import { DialogConfirmComponent } from '../modals/dialog-confirm/dialog-confirm.component';
import { DialogShareBoardComponent } from '../modals/dialog-share-board/dialog-share-board.component';

@Component({
	selector: 'app-list-boards',
	templateUrl: './list-boards.component.html',
	styles: [],
})
export class ListBoardsComponent implements OnInit {
	@Input() boards: DashboardModel[] = [];
	@Output() refresh = new EventEmitter<void>();

	constructor(
		private api: AlisonService,
		private dialogService: NbDialogService,
		private toastrService: NbToastrService
	) {}

	ngOnInit(): void {}

	openEditBoardDialog(board: DashboardModel) {
		this.dialogService
			.open(DialogBoardComponent, {
				context: {
					editMode: true,
					board: board,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.refresh.emit();
			});
	}

	openShareBoardDialog(board: DashboardModel) {
		this.dialogService
			.open(DialogShareBoardComponent, {
				context: {
					board: board,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.refresh.emit();
			});
	}

	openDeleteBoardDialog(board: DashboardModel) {
		this.dialogService
			.open(DialogConfirmComponent, {
				context: {
					title: '¿Estás seguro?',
					content: 'Se archivará el tablero, podrás recuperarlo si lo deseas más tarde.',
					icon: 'alert-circle-outline',
					showCancelButton: true,
					confirmButtonText: '¡Sí, archívalo!',
				},
			})
			.onClose.subscribe((result) => {
				if (result)
					this.api.dashboard.delete(board.id).subscribe(
						(response: any) => {
							if (response.statusCode == 200) {
								this.toastrService.success(null, `Se archivó el tablero`, { icon: 'archive-outline' });
								this.refresh.emit();
							}
						},
						(error) => {
							this.toastrService.danger(null, `No se pudo archivar el tablero`);
							console.log(error);
						}
					);
			});
	}
}
