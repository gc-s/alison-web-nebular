import { Component, Input, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { ChecklistModel } from 'src/app/models/checklist/checklist.model';
import { AlisonService } from 'src/app/services/alison.service';
import { DialogCriterioComponent } from '../modals/dialog-criterio/dialog-criterio.component';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styles: []
})
export class ChecklistComponent implements OnInit {
  @Input() dashboardKey: string;
  @Input() idList: number;
  @Input() idTask: number;
  @Input() checklist: ChecklistModel;

  constructor(private dialogService: NbDialogService, private api: AlisonService) { }

  ngOnInit(): void {
  }

  openCriterioDialog() {
    this.dialogService.open(DialogCriterioComponent, {
      context: {
        editMode: false,
        dashboardKey: this.dashboardKey,
        idList: this.idList,
        idTask: this.idTask,
        idChecklist: this.checklist.id
      },
    }).onClose.subscribe((result) => {
      if (result) this.getChecklist();
    });
  }

  getChecklist() {
    this.api.checklist.get(this.dashboardKey, this.idList, this.idTask, this.checklist.id).subscribe((response: any) => {
      //console.log(response);
      this.checklist = response;
    });
  }

}
