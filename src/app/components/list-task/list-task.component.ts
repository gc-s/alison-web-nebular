import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbDialogService, NbMenuBag, NbMenuItem, NbMenuService, NbToastrService } from '@nebular/theme';
import { distinct, filter, map } from 'rxjs/operators';
import { TaskModel } from 'src/app/models/task/task.model';
import { AlisonService } from 'src/app/services/alison.service';
import { DialogConfirmComponent } from '../modals/dialog-confirm/dialog-confirm.component';
import { DialogLabelTaskComponent } from '../modals/dialog-label-task/dialog-label-task.component';
import { DialogTaskComponent } from '../modals/dialog-task/dialog-task.component';

@Component({
	selector: 'app-task',
	templateUrl: './list-task.component.html',
	styles: [],
})
export class ListTaskComponent implements OnInit {
	@Input() idList: number;
	@Input() dashboardKey: string;
	@Input() tasks: TaskModel[];
	@Output() refresh = new EventEmitter<void>();

	private selectMenu: any;
	private selectedTask: TaskModel;

	menuTask: NbMenuItem[] = [
		{ title: 'Etiquetas', icon: 'pricetags-outline' },
		{ title: 'Editar tarea', icon: 'edit-outline' },
		{ title: 'Archivar tarea', icon: 'trash-2-outline' },
	];

	constructor(
		private api: AlisonService,
		private toastrService: NbToastrService,
		private dialogService: NbDialogService,
		private nbMenuService: NbMenuService
	) {}

	ngOnInit(): void {
		this.selectMenu = this.nbMenuService
			.onItemClick()
			.pipe(
				filter(({ tag }) => tag === 'task-menu'),
				distinct((menu) => menu),
				map(({ item: { icon } }) => icon)
			)
			.subscribe((icon) => {
				switch (icon) {
					case 'pricetags-outline':
						console.log('call');
						this.openLabelTaskDialog(this.selectedTask);
						break;
					case 'edit-outline':
						this.openEditTaskDialog(this.selectedTask);
						break;
					case 'trash-2-outline':
						this.openDeleteTaskDialog(this.selectedTask);
						break;
				}
			});
	}

	ngOnDestroy(): void {
		this.selectMenu.unsubscribe();
	}

	private openLabelTaskDialog(task: TaskModel) {
		this.dialogService
			.open(DialogLabelTaskComponent, {
				context: {
					task: task,
					idList: this.idList,
					dashboardKey: this.dashboardKey,
				},
			})
			.onClose.subscribe(() => {
				this.refresh.emit();
			});
	}

	openEditTaskDialog(task: TaskModel) {
		this.dialogService
			.open(DialogTaskComponent, {
				context: {
					editMode: true,
					task: task,
					dashboardKey: this.dashboardKey,
					idList: this.idList,
					members: task.members,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.refresh.emit();
			});
	}

	openDeleteTaskDialog(task: TaskModel) {
		this.dialogService
			.open(DialogConfirmComponent, {
				context: {
					title: '¿Estás seguro?',
					content: 'Se archivará la tarea, podrás recuperarla más tarde.',
					icon: 'alert-circle-outline',
					showCancelButton: true,
					confirmButtonText: '¡Sí, archívalo!',
				},
			})
			.onClose.subscribe((result) => {
				if (result)
					this.api.task.delete(this.dashboardKey, this.idList, task.id).subscribe(
						(response: any) => {
							if (response.statusCode == 200) {
								this.toastrService.success(null, `Se archivó la tarea`, { icon: 'archive-outline' });
								this.refresh.emit();
							}
						},
						(error) => {
							this.toastrService.danger(null, `No se pudo archivar la tarea`);
							console.log(error);
						}
					);
			});
	}

	setTask(task: TaskModel) {
		this.selectedTask = task;
	}

	onTaskDrop(a: any) {
		console.log(a);
	}
}
