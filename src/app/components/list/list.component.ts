import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbDialogService, NbMenuItem, NbMenuService, NbToastrService } from '@nebular/theme';
import { filter, map } from 'rxjs/operators';
import { ListModel } from 'src/app/models/list/list.model';
import { AlisonService } from 'src/app/services/alison.service';
import { DialogConfirmComponent } from '../modals/dialog-confirm/dialog-confirm.component';
import { DialogListComponent } from '../modals/dialog-list/dialog-list.component';
import { DialogTaskComponent } from '../modals/dialog-task/dialog-task.component';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styles: [],
})
export class ListComponent implements OnInit {
	@Input() lists: ListModel[];
	@Input() dashboardKey: string;
	@Output() refresh = new EventEmitter<void>();

	private selectMenu: any;

	private selectedList: ListModel;

	menuList: NbMenuItem[] = [
		{ title: 'Añadir tarea', icon: 'plus-outline' },
		{ title: 'Editar lista', icon: 'edit-outline' },
		{ title: 'Archivar lista', icon: 'trash-2-outline' },
	];

	constructor(
		private api: AlisonService,
		private toastrService: NbToastrService,
		private dialogService: NbDialogService,
		private nbMenuService: NbMenuService
	) {}

	ngOnInit(): void {
		this.selectMenu = this.nbMenuService
			.onItemClick()
			.pipe(
				filter(({ tag }) => tag === 'list-menu'),
				map(({ item: { icon } }) => icon)
			)
			.subscribe((icon) => {
				switch (icon) {
					case 'plus-outline':
						if (this.selectedList.wip > this.selectedList.tasks.length || this.selectedList.wip == 0) {
							this.openTaskDialog(this.selectedList.id);
						} else this.toastrService.danger(null, `No se pueden añadir tareas WIP superado`);
						break;
					case 'edit-outline':
						this.openEditListDialog(this.selectedList);
						break;
					case 'trash-2-outline':
						this.openDeleteListDialog(this.selectedList);
						break;
				}
			});
	}

	ngOnDestroy(): void {
		this.selectMenu.unsubscribe();
	}

	openTaskDialog(idList: number) {
		this.dialogService
			.open(DialogTaskComponent, {
				context: {
					dashboardKey: this.dashboardKey,
					idList: idList,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.refresh.emit();
			});
	}

	openEditListDialog(list: ListModel) {
		this.dialogService
			.open(DialogListComponent, {
				context: {
					editMode: true,
					list: list,
					dashboardKey: this.dashboardKey,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.refresh.emit();
			});
	}

	openDeleteListDialog(list: ListModel) {
		this.dialogService
			.open(DialogConfirmComponent, {
				context: {
					title: '¿Estás seguro?',
					content: 'Se archivará la lista, podrás recuperarla más tarde.',
					icon: 'alert-circle-outline',
					showCancelButton: true,
					confirmButtonText: '¡Sí, archívalo!',
				},
			})
			.onClose.subscribe((result) => {
				if (result)
					this.api.list.delete(this.dashboardKey, list.id).subscribe(
						(response: any) => {
							if (response.statusCode == 200) {
								this.toastrService.success(null, `Se archivó la lista`, { icon: 'archive-outline' });
								this.refresh.emit();
							}
						},
						(error) => {
							this.toastrService.danger(null, `No se pudo archivar la lista`);
							console.log(error);
						}
					);
			});
	}

	setList(list: ListModel) {
		this.selectedList = list;
	}

	trackByFn(index, item) {
		return index; // or item.id
	}
}
