import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbMenuService, NbToastrService } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';
import { filter, map } from 'rxjs/operators';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styles: [],
})
export class SidebarComponent implements OnInit, AfterViewInit {
	@Input() items: NbMenuItem[] = [];

	menuItems: NbMenuItem[] = [
		{
			title: 'Mi perfil',
			icon: 'person-outline',
			expanded: false,
			children: [
				{
					title: 'Preferencias',
					icon: 'settings-2-outline',
				},
				{
					title: 'Privacy Policy',
					icon: 'book-open-outline',
				},
				{
					title: 'Cerrar sesión',
					icon: 'unlock-outline',
				},
			],
		},
	];

	constructor(
		private sidebarService: NbSidebarService,
		private nbMenuService: NbMenuService,
		private toastrService: NbToastrService,
		private api: AlisonService
	) {}

	ngAfterViewInit(): void {}

	ngOnInit(): void {
		this.menuItems = this.menuItems.concat(this.items);
		this.nbMenuService
			.onItemClick()
			.pipe(
				filter(({ tag }) => tag === 'sidebar-menu'),
				map(({ item: { icon } }) => icon)
			)
			.subscribe((icon) => {
				switch (icon) {
					case 'settings-2-outline':
						break;
					case 'book-open-outline':
						this.toastrService.info(null, `Términos y condiciones`);
						break;
					case 'unlock-outline':
						this.api.auth.logout();
						break;
					default:
						break;
				}
				this.sidebarService.compact('menu-sidebar');
			});
	}

	toggle() {
		this.sidebarService.toggle(false, 'left');
	}

	toggleCompact() {
		this.sidebarService.toggle(true, 'right');
	}
}
