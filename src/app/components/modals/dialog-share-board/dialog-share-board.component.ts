import { Component, Input, AfterViewInit } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlisonService } from 'src/app/services/alison.service';
import { DashboardModel } from 'src/app/models/dashboard/dashboard.model';

@Component({
	selector: 'app-dialog-share-board',
	templateUrl: './dialog-share-board.component.html',
	styles: [],
})
export class DialogShareBoardComponent implements AfterViewInit {
	@Input() board: DashboardModel;
	shareFG: FormGroup;
	submitted: boolean = false;
	generated: boolean = false;

	constructor(
		protected ref: NbDialogRef<DialogShareBoardComponent>,
		private fb: FormBuilder,
		private api: AlisonService,
		private toastrService: NbToastrService
	) {
		this.buildShareForm();
	}

	ngAfterViewInit(): void {}

	cancel() {
		this.ref.close(false);
	}

	submit() {
		this.submitted = true;
		this.shareBoard();
	}

	copyLink() {
		document.addEventListener('copy', (e: ClipboardEvent) => {
			e.clipboardData.setData('text/plain', this.inputLink.value);
			e.preventDefault();
			document.removeEventListener('copy', null);
		});
		document.execCommand('copy');
	}

	shareBoard() {
		if (this.shareFG.valid) {
			this.api.members.joinForm(this.board.id, this.inputEmail.value).subscribe(
				(response: any) => {
					if (response.statusCode == 201) {
						this.toastrService.success(null, `Se añadió colaborador`);
						this.ref.close(true);
					} else if (response.statusCode == 200) {
						this.submitted = false;
						this.toastrService.info(null, `El usuario ya colabora en el tablero`);
					}
				},
				(error) => {
					this.submitted = false;
					console.log(error);
					this.toastrService.danger(null, error.error.message);
				}
			);
		} else {
			this.submitted = false;
			return Object.values(this.shareFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	generateLink() {
		this.api.members.getCode(this.board.key, 1, 60).subscribe((response: any) => {
			this.generated = true;
			this.inputLink.setValue(
				`${window.location.href.substr(0, window.location.href.length - 5)}/join/${response.code}`
			);
		});
	}

	buildShareForm() {
		this.shareFG = this.fb.group({
			email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
			link: [''],
		});
	}

	get inputEmail() {
		return this.shareFG.get('email');
	}

	get inputLink() {
		return this.shareFG.get('link');
	}
}
