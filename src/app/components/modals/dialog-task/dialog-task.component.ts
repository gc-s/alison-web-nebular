import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NbDialogRef, NbDialogService, NbToastrService } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';
import { ApiResponse } from 'src/models/response';
import { DialogMembersComponent } from '../dialog-members/dialog-members.component';
import { Member } from 'src/models/member';
import { Comment } from 'src/models/comment';
import { ChecklistModel } from "src/app/models/checklist/checklist.model";
import { DialogLabelTaskComponent } from '../dialog-label-task/dialog-label-task.component';
import { TaskModel } from 'src/app/models/task/task.model';
import { DialogChecklistComponent } from '../dialog-checklist/dialog-checklist.component';

@Component({
	selector: 'app-dialog-task',
	templateUrl: './dialog-task.component.html',
	styles: [],
})
export class DialogTaskComponent implements OnInit, OnDestroy {
	@Input() editMode: boolean = false;
	@Input() dashboardKey: string;
	@Input() idList: number;
	@Input() task: TaskModel;
	@Input() members: Member[];
	editedTask: TaskModel;
	comments: Comment[] = [];
	checklists: ChecklistModel[] = [];
	taskFG: FormGroup;
	submitted: boolean = false;
	size: number;
	real: number;

	inputSize: FormControl;
	inputReal: FormControl;
	inputStartDate: FormControl;
	inputLimitDate: FormControl;

	constructor(
		protected ref: NbDialogRef<DialogTaskComponent>,
		private fb: FormBuilder,
		private api: AlisonService,
		private dialogService: NbDialogService,
		private toastrService: NbToastrService
	) { }

	ngOnInit(): void {
		this.buildTaskForm();
		if (this.editMode) {
			this.editedTask = this.task;
			this.real = this.task.realSize;
			this.size = this.task.estimatedSize;
			this.inputSize = new FormControl(this.size, Validators.pattern('[0-9]+'));
			this.inputReal = new FormControl(this.real, Validators.pattern('[0-9]+'));
			this.inputStartDate = new FormControl(this.task.startDate);
			this.inputLimitDate = new FormControl(this.task.endDate);
			this.getComments();
			this.getChecklists();
		}
	}

	ngOnDestroy(): void {
		if (this.editMode) this.updateTask();
	}

	cancel() {
		this.ref.close(true);
	}

	submit() {
		this.submitted = true;
		if (this.editMode) this.updateTask();
		else this.createTask();
	}

	getComments() {
		this.api.comment.getAll(this.dashboardKey, this.idList, this.task.id).subscribe((response: any) => {
			//console.log(response);
			this.comments = response;
		});
	}

	getChecklists() {
		this.api.checklist.getAll(this.dashboardKey, this.idList, this.task.id).subscribe((response: any) => {
			//console.log(response);
			this.checklists = response;
		});
	}

	createTask() {
		if (this.taskFG.valid) {
			this.api.task
				.create(this.dashboardKey, this.idList, this.inputTitle.value, this.inputDescription.value)
				.subscribe(
					(response: any) => {
						if (response.statusCode == 201) {
							this.toastrService.success(null, `Se añadió la tarea`);
							this.ref.close(true);
						}
					},
					(error) => {
						this.submitted = false;
						this.toastrService.danger(null, `No se pudo añadir la tarea`);
						console.log(error);
					}
				);
		} else {
			this.submitted = false;
			return Object.values(this.taskFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	updateTask() {
		// if (this.editedTask != this.task) {
		let endDate: string = this.task.endDate ? this.task.endDate.toString() : null;
		console.log(this.idList);
		this.api.task
			.update(
				this.dashboardKey,
				this.idList,
				this.task.id,
				this.inputTitle.value,
				this.inputDescription.value,
				this.inputStartDate.value,
				this.inputLimitDate.value,
				endDate,
				this.inputSize.value,
				this.inputReal.value
			)
			.subscribe(
				(response: any) => {
					if (response.statusCode == 200) {
						this.toastrService.success(null, `Se editó la tarea`);
						this.ref.close(true);
					}
				},
				(error) => {
					this.submitted = false;
					this.toastrService.danger(null, `No se pudo editar la tarea`);
					console.log(error);
				}
			);
		//}
	}

	finishedTask() {
		this.submitted = true;
		this.api.task.finished(this.dashboardKey, this.idList, this.task.id).subscribe(
			(response: ApiResponse) => {
				this.submitted = false;
				if (response.statusCode == 200) {
					this.task.endDate = new Date();
					this.toastrService.success(null, response.message);
				}
			},
			(error) => {
				this.submitted = false;
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	unfinishedTask() {
		this.submitted = true;
		this.api.task.unfinished(this.dashboardKey, this.idList, this.task.id).subscribe(
			(response: ApiResponse) => {
				this.submitted = false;
				if (response.statusCode == 200) {
					this.task.endDate = null;
					this.toastrService.success(null, response.message);
				}
			},
			(error) => {
				this.submitted = false;
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	openMembersDialog() {
		this.dialogService.open(DialogMembersComponent, {
			context: {
				inTask: true,
				task: this.task,
				dashboardKey: this.dashboardKey,
				members: this.members,
			},
		});
	}

	openChecklistDialog() {
		this.dialogService.open(DialogChecklistComponent, {
			context: {
				editMode: false,
				dashboardKey: this.dashboardKey,
				idList: this.idList,
				idTask: this.task.id
			},
		}).onClose.subscribe((result) => {
			if (result) this.getChecklists();
		});
	}

	getTask() {
		this.api.task.get(this.dashboardKey, this.idList, this.task.id).subscribe((response: any) => {
			if (!response.miembros[0].idUser) response.miembros = [];
			response.miembros = response.miembros.map((member) => {
				member.idUser.rol = member.rol;
				delete member.rol;
				return member.idUser;
			});
			this.task = response as TaskModel;
		});
	}

	sendComment(comment: string) {
		this.api.comment.create(this.dashboardKey, this.idList, this.task.id, comment).subscribe(
			(response: ApiResponse) => {
				this.submitted = false;
				if (response.statusCode == 201) {
					this.getComments();
					this.toastrService.success(null, response.message);
				}
			},
			(error) => {
				this.submitted = false;
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	openLabelTaskDialog() {
		this.dialogService
			.open(DialogLabelTaskComponent, {
				context: {
					task: this.task,
					idList: this.idList,
					dashboardKey: this.dashboardKey,
				},
			})
			.onClose.subscribe(() => {
				//this.getAllList();
			});
	}

	buildTaskForm() {
		if (this.editMode) {
			this.taskFG = this.fb.group({
				title: [this.task.title, [Validators.required, Validators.minLength(2), Validators.maxLength(120)]],
				description: [this.task.description, [Validators.required, Validators.minLength(2)]],
			});
			Object.values(this.taskFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		} else
			this.taskFG = this.fb.group({
				title: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(120)]],
				description: ['', [Validators.required, Validators.minLength(2)]],
			});
	}

	get inputTitle() {
		return this.taskFG.get('title');
	}

	get inputDescription() {
		return this.taskFG.get('description');
	}
}
