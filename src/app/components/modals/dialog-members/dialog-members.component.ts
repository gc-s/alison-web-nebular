import { Component, Input, OnInit } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { Member } from 'src/models/member';
import { Rol } from 'src/models/rol';
import { ApiResponse } from 'src/models/response';
import { AlisonService } from 'src/app/services/alison.service';
import { TaskModel } from 'src/app/models/task/task.model';

@Component({
	selector: 'app-dialog-members',
	templateUrl: './dialog-members.component.html',
	styles: [],
})
export class DialogMembersComponent implements OnInit {
	@Input() dashboardKey: string;
	@Input() members: Member[] = [];
	@Input() inTask: boolean = false;
	@Input() task: TaskModel;
	assigned: Member[];

	constructor(
		protected ref: NbDialogRef<DialogMembersComponent>,
		private api: AlisonService,
		private toastrService: NbToastrService
	) {}

	ngOnInit(): void {
		if (this.inTask) {
			this.assigned = this.task.members;
			this.members.map((mem) => {
				mem.assigned = false;
			});
			this.assigned.map((mem) => {
				this.members.find((m) => {
					if (m.id === mem.id) {
						m.assigned = true;
					}
				});
			});
		}
	}

	ngOnDestroy(): void {
		this.assigned = null;
		this.ref.close();
	}

	setUser(user: Member) {
		this.api.members.assignRol(this.dashboardKey, user.id, Rol.User).subscribe(
			(response: ApiResponse) => {
				if (response.statusCode == 200) {
					this.members[this.members.indexOf(user)].rol = Rol.User;
					this.toastrService.success(null, response.message);
				}
			},
			(error) => {
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	setAdmin(user: Member) {
		this.api.members.assignRol(this.dashboardKey, user.id, Rol.Admin).subscribe(
			(response: ApiResponse) => {
				if (response.statusCode == 200) {
					this.members[this.members.indexOf(user)].rol = Rol.Admin;
					this.toastrService.success(null, response.message);
				}
			},
			(error) => {
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	remove(user: Member) {
		this.api.members.removeUser(this.dashboardKey, user.id).subscribe(
			(response: ApiResponse) => {
				if (response.statusCode == 200) {
					this.members.splice(this.members.indexOf(user), 1);
					this.toastrService.info(null, response.message, { icon: 'trash-2-outline' });
				}
			},
			(error) => {
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	assignMember(user: Member) {
		this.api.assignments.assignMemberToTasks(this.dashboardKey, this.task.id, user.id).subscribe(
			(response: ApiResponse) => {
				if (response.statusCode == 201) {
					this.members[this.members.indexOf(user)].assigned = true;
					this.assigned.push(user);
					this.toastrService.success(null, response.message);
				}
			},
			(error) => {
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	unassignMember(user: Member) {
		this.api.assignments.unassignMemberToTasks(this.dashboardKey, this.task.id, user.id).subscribe(
			(response: ApiResponse) => {
				if (response.statusCode == 200) {
					this.members[this.members.indexOf(user)].assigned = false;
					this.assigned.some((member, index) => {
						if (member.id == user.id) this.assigned.splice(index, 1);
					});
					this.toastrService.info(null, response.message, { icon: 'trash-2-outline' });
				}
			},
			(error) => {
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}
}
