import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';

@Component({
  selector: 'app-dialog-criterio',
  templateUrl: './dialog-criterio.component.html',
  styles: []
})
export class DialogCriterioComponent implements OnInit {
  @Input() editMode: boolean = false;
  @Input() dashboardKey: string;
  @Input() idList: number;
  @Input() idTask: number;
  @Input() idChecklist: number;

  criterioFG: FormGroup;
  submitted: boolean = false;

  constructor(
    protected ref: NbDialogRef<DialogCriterioComponent>,
    private fb: FormBuilder,
    private api: AlisonService,
    private toastrService: NbToastrService
  ) { }

  ngOnInit(): void {
    this.buildCriterioForm();
  }

  cancel() {
    this.ref.close(false);
  }

  submit() {
    this.submitted = true;
    if (this.editMode) this.createCriterio();
    else this.createCriterio();
  }

  createCriterio() {
    if (this.criterioFG.valid) {
      this.api.criterio.create(this.dashboardKey, this.idList, this.idTask, this.idChecklist, this.inputIndicator.value).subscribe(
        (response: any) => {
          if (response.statusCode == 201) {
            this.toastrService.success(null, `Se añadió el criterio`);
            this.ref.close(true);
          }
        },
        (error) => {
          this.submitted = false;
          this.toastrService.danger(null, `No se pudo añadir el criterio`);
          console.log(error);
        }
      );
    } else {
      this.submitted = false;
      return Object.values(this.criterioFG.controls).forEach((control) => {
        control.markAsDirty();
      });
    }
  }

  buildCriterioForm() {
    if (this.editMode) {
      /*this.criterioFG = this.fb.group({
        name: [this.label.name, [Validators.required, Validators.minLength(2)]],
      });
      Object.values(this.criterioFG.controls).forEach((control) => {
        control.markAsDirty();
      });*/
    } else
      this.criterioFG = this.fb.group({
        indicator: ['', [Validators.required, Validators.minLength(1)]],
      });
  }

  get inputIndicator() {
    return this.criterioFG.get('indicator');
  }

}
