import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';

@Component({
	selector: 'app-dialog-confirm',
	templateUrl: './dialog-confirm.component.html',
	styles: [],
})
export class DialogConfirmComponent {
	@Input() title: string;
	@Input() content: string;
	@Input() icon: string;
	@Input() showCancelButton: boolean = false;
	@Input() confirmButtonStatus: string = 'success';
	@Input() cancelButtonStatus: string = 'danger';
	@Input() confirmButtonText: string = 'Aceptar';
	@Input() cancelButtonText: string = 'Cancelar';

	constructor(protected ref: NbDialogRef<DialogConfirmComponent>, private api: AlisonService) {}

	cancel() {
		this.ref.close(false);
	}

	submit() {
		this.ref.close(true);
	}
}
