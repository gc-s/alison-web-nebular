import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';
import { ListModel } from 'src/app/models/list/list.model';

@Component({
	selector: 'app-dialog-list',
	templateUrl: './dialog-list.component.html',
	styles: [],
})
export class DialogListComponent implements OnInit {
	@Input() editMode: boolean = false;
	@Input() dashboardKey: string;
	@Input() list: ListModel;
	listFG: FormGroup;
	submitted: boolean = false;

	constructor(
		protected ref: NbDialogRef<DialogListComponent>,
		private fb: FormBuilder,
		private api: AlisonService,
		private toastrService: NbToastrService
	) {}

	ngOnInit(): void {
		this.buildListForm();
	}

	cancel() {
		this.ref.close(false);
	}

	submit() {
		this.submitted = true;
		if (this.editMode) this.updateList();
		else this.createList();
	}

	createList() {
		if (this.listFG.valid) {
			this.api.list.create(this.dashboardKey, this.inputName.value, this.inputWip.value).subscribe(
				(response: any) => {
					if (response.statusCode == 201) {
						this.toastrService.success(null, `Se añadió la lista`);
						this.ref.close(true);
					}
				},
				(error) => {
					this.submitted = false;
					this.toastrService.danger(null, `No se pudo añadir la lista`);
					console.log(error);
				}
			);
		} else {
			this.submitted = false;
			return Object.values(this.listFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	updateList() {
		if (this.listFG.valid && this.list.id > 0) {
			if (this.inputName.value == this.list.name && this.inputWip.value == this.list.wip) this.ref.close(false);
			else {
				console.log(this.listFG.value);
				this.api.list.update(this.dashboardKey, this.list.id, this.inputName.value, this.inputWip.value).subscribe(
					(response: any) => {
						console.log(response);
						if (response.statusCode == 200) {
							this.toastrService.success(null, `Se editó la lista`);
							this.ref.close(true);
						}
					},
					(error) => {
						this.submitted = false;
						console.log(error);
						this.toastrService.danger(null, `No se pudo editar la lista`);
					}
				);
			}
		} else {
			this.submitted = false;
			return Object.values(this.listFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	buildListForm() {
		if (this.editMode) {
			this.listFG = this.fb.group({
				name: [this.list.name, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
				wip: [this.list.wip, [Validators.required, Validators.pattern('[0-9]+')]],
			});
			Object.values(this.listFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		} else
			this.listFG = this.fb.group({
				name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
				wip: ['', [Validators.required, Validators.pattern('[0-9]+')]],
			});
	}

	get inputName() {
		return this.listFG.get('name');
	}

	get inputWip() {
		return this.listFG.get('wip');
	}
}
