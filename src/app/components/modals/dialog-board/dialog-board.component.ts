import { Component, Input, OnInit } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AlisonService } from 'src/app/services/alison.service';
import { DashboardModel } from 'src/app/models/dashboard/dashboard.model';

@Component({
	selector: 'app-dialog-board',
	templateUrl: './dialog-board.component.html',
	styles: [],
})
export class DialogBoardComponent implements OnInit {
	@Input() editMode: boolean = false;
	@Input() board: DashboardModel;
	boardFG: FormGroup;
	submitted: boolean = false;

	constructor(
		protected ref: NbDialogRef<DialogBoardComponent>,
		private fb: FormBuilder,
		private api: AlisonService,
		private toastrService: NbToastrService
	) {}

	ngOnInit(): void {
		this.buildBoardForm();
	}

	cancel() {
		this.ref.close(false);
	}

	submit() {
		this.submitted = true;
		if (this.editMode) this.updateBoard();
		else this.createBoard();
	}

	createBoard() {
		if (this.boardFG.valid) {
			this.api.dashboard.create(this.inputName.value, this.inputBg.value).subscribe(
				(response: any) => {
					if (response.statusCode == 201) {
						this.toastrService.success(null, `Se añadió el tablero`);
						this.ref.close(true);
					}
				},
				(error) => {
					this.submitted = false;
					this.toastrService.danger(null, `No se pudo añadir el tablero`);
					console.log(error);
				}
			);
		} else {
			this.submitted = false;
			return Object.values(this.boardFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	updateBoard() {
		if (this.boardFG.valid && this.board.id > 0) {
			if (this.inputName.value == this.board.name && this.inputBg.value == this.board.background.substr(1))
				this.ref.close(false);
			else {
				console.log(this.boardFG.value);
				this.api.dashboard.update(this.board.id, this.inputName.value, this.inputBg.value).subscribe(
					(response: any) => {
						console.log(response);
						if (response.statusCode == 200) {
							this.toastrService.success(null, `Se editó el tablero`);
							this.ref.close(true);
						}
					},
					(error) => {
						console.log(error);
						this.submitted = false;
						this.toastrService.danger(null, `No se pudo editar el tablero`);
					}
				);
			}
		} else {
			this.submitted = false;
			return Object.values(this.boardFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	buildBoardForm() {
		if (this.editMode) {
			this.boardFG = this.fb.group({
				name: [this.board.name, [Validators.required, Validators.minLength(2)]],
				bg: [
					this.board.background.substr(1),
					[
						Validators.required,
						Validators.pattern('([0-9a-fA-F]{3}){1,2}'),
						Validators.minLength(3),
						Validators.maxLength(6),
					],
				],
			});
			Object.values(this.boardFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		} else
			this.boardFG = this.fb.group({
				name: ['', [Validators.required, Validators.minLength(2)]],
				bg: [
					'',
					[
						Validators.required,
						Validators.pattern('([0-9a-fA-F]{3}){1,2}'),
						Validators.minLength(3),
						Validators.maxLength(6),
					],
				],
			});
	}

	get inputName() {
		return this.boardFG.get('name');
	}

	get inputBg() {
		return this.boardFG.get('bg');
	}
}
