import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlisonService } from 'src/app/services/alison.service';
import { LabelModel } from 'src/app/models/label/label.model';

@Component({
	selector: 'app-dialog-label',
	templateUrl: './dialog-label.component.html',
	styles: [],
})
export class DialogLabelComponent implements OnInit {
	@Input() editMode: boolean = false;
	@Input() dashboardKey: string;
	@Input() label: LabelModel;
	labelFG: FormGroup;
	submitted: boolean = false;

	constructor(
		protected ref: NbDialogRef<DialogLabelComponent>,
		private fb: FormBuilder,
		private api: AlisonService,
		private toastrService: NbToastrService
	) {}

	ngOnInit(): void {
		this.buildLabelForm();
	}

	cancel() {
		this.ref.close(false);
	}

	submit() {
		this.submitted = true;
		if (this.editMode) this.updateLabel();
		else this.createLabel();
	}

	createLabel() {
		if (this.labelFG.valid) {
			this.api.label.create(this.dashboardKey, this.inputName.value, this.inputBg.value).subscribe(
				(response: any) => {
					if (response.statusCode == 201) {
						this.toastrService.success(null, `Se añadió la etiqueta`);
						this.ref.close(true);
					}
				},
				(error) => {
					this.submitted = false;
					this.toastrService.danger(null, `No se pudo añadir la etiqueta`);
					console.log(error);
				}
			);
		} else {
			this.submitted = false;
			return Object.values(this.labelFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	updateLabel() {
		if (this.labelFG.valid && this.label.id > 0) {
			if (this.inputName.value == this.label.name && this.inputBg.value == this.label.color.substr(1))
				this.ref.close(false);
			else {
				console.log(this.labelFG.value);
				this.api.label.update(this.dashboardKey, this.label.id, this.inputName.value, this.inputBg.value).subscribe(
					(response: any) => {
						console.log(response);
						if (response.statusCode == 200) {
							this.toastrService.success(null, `Se editó la etiqueta`);
							this.ref.close(true);
						}
					},
					(error) => {
						this.submitted = false;
						console.log(error);
						this.toastrService.danger(null, `No se pudo editar la etiqueta`);
					}
				);
			}
		} else {
			this.submitted = false;
			return Object.values(this.labelFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		}
	}

	buildLabelForm() {
		if (this.editMode) {
			this.labelFG = this.fb.group({
				name: [this.label.name, [Validators.required, Validators.minLength(2)]],
				bg: [
					this.label.color.substr(1),
					[
						Validators.required,
						Validators.pattern('([0-9a-fA-F]{3}){1,2}'),
						Validators.minLength(3),
						Validators.maxLength(6),
					],
				],
			});
			Object.values(this.labelFG.controls).forEach((control) => {
				control.markAsDirty();
			});
		} else
			this.labelFG = this.fb.group({
				name: ['', [Validators.required, Validators.minLength(2)]],
				bg: [
					'',
					[
						Validators.required,
						Validators.pattern('([0-9a-fA-F]{3}){1,2}'),
						Validators.minLength(3),
						Validators.maxLength(6),
					],
				],
			});
	}

	get inputName() {
		return this.labelFG.get('name');
	}

	get inputBg() {
		return this.labelFG.get('bg');
	}
}
