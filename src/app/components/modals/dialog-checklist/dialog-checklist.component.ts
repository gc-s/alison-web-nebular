import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';
import { ChecklistModel } from '../../../models/checklist/checklist.model';

@Component({
  selector: 'app-dialog-checklist',
  templateUrl: './dialog-checklist.component.html',
  styles: []
})
export class DialogChecklistComponent implements OnInit {
  @Input() editMode: boolean = false;
  @Input() dashboardKey: string;
  @Input() idList: number;
  @Input() idTask: number;

  checklistFG: FormGroup;
  submitted: boolean = false;

  constructor(
    protected ref: NbDialogRef<DialogChecklistComponent>,
    private fb: FormBuilder,
    private api: AlisonService,
    private toastrService: NbToastrService
  ) { }

  ngOnInit(): void {
    this.buildChecklistForm();
  }

  cancel() {
    this.ref.close(false);
  }

  submit() {
    this.submitted = true;
    if (this.editMode) this.createChecklist();
    else this.createChecklist();
  }

  createChecklist() {
    if (this.checklistFG.valid) {
      this.api.checklist.create(this.dashboardKey, this.idList, this.idTask, this.inputName.value).subscribe(
        (response: any) => {
          if (response.statusCode == 201) {
            this.toastrService.success(null, `Se añadió el checklist`);
            this.ref.close(true);
          }
        },
        (error) => {
          this.submitted = false;
          this.toastrService.danger(null, `No se pudo añadir el checklist`);
          console.log(error);
        }
      );
    } else {
      this.submitted = false;
      return Object.values(this.checklistFG.controls).forEach((control) => {
        control.markAsDirty();
      });
    }
  }

  buildChecklistForm() {
    if (this.editMode) {
      /*this.checklistFG = this.fb.group({
        name: [this.label.name, [Validators.required, Validators.minLength(2)]],
      });
      Object.values(this.checklistFG.controls).forEach((control) => {
        control.markAsDirty();
      });*/
    } else
      this.checklistFG = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(1)]],
      });
  }

  get inputName() {
    return this.checklistFG.get('name');
  }

}
