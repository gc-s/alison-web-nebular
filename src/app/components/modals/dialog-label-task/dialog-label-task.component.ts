import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';
import { TaskModel } from 'src/app/models/task/task.model';
import { LabelModel } from 'src/app/models/label/label.model';

@Component({
	selector: 'app-dialog-label-task',
	templateUrl: './dialog-label-task.component.html',
	styles: [],
})
export class DialogLabelTaskComponent implements OnInit {
	@Input() dashboardKey: string;
	@Input() idList: number;
	@Input() task: TaskModel;
	labels: LabelModel[] = [];

	constructor(
		protected ref: NbDialogRef<DialogLabelTaskComponent>,
		private api: AlisonService,
		private toastrService: NbToastrService
	) {}

	ngOnInit(): void {
		this.getLabels();
	}

	ngOnDestroy(): void {
		this.task = null;
		this.ref.close(false);
	}

	cancel() {
		this.ref.close(false);
	}

	getLabels() {
		this.api.label.getAll(this.dashboardKey).subscribe((response: LabelModel[]) => {
			response.forEach((label) => {
				if (this.task.labels.find((eti) => eti.id == label.id)) label.check = true;
			});
			this.labels = response;
		});
	}

	switchTag(label: LabelModel, checked: boolean) {
		if (checked) {
			this.api.assignments.assignLabel(this.dashboardKey, this.task.id, label.id).subscribe((response) => {
				console.log(response);
				this.toastrService.success(null, `Se añadió la etiqueta a la tarea`);
			});
		} else {
			this.api.assignments.unassignLabel(this.dashboardKey, this.task.id, label.id).subscribe((response) => {
				console.log(response);
				this.toastrService.info(null, `Se retiró la etiqueta de la tarea`, { icon: 'checkmark-outline' });
			});
		}
	}
}
