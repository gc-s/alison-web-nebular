import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { Comment } from 'src/models/comment';
import { ApiResponse } from 'src/models/response';
import { AlisonService } from 'src/app/services/alison.service';

@Component({
	selector: 'app-comment',
	templateUrl: './comment.component.html',
	styles: [],
})
export class CommentComponent implements OnInit {
	@Input() dashboardKey: string;
	@Input() idList: number;
	@Input() idTask: number;
	@Input() comment: Comment;
	@Output() onDeleteComment = new EventEmitter<boolean>();
	readonly = true;

	constructor(private api: AlisonService, private toastrService: NbToastrService) { }

	ngOnInit(): void { }

	editComment(comment: Comment, newComment: string) {
		console.log(newComment);
		this.api.comment.update(this.dashboardKey, this.idList, this.idTask, comment.id, newComment).subscribe(
			(response: ApiResponse) => {
				if (response.statusCode == 200) {
					comment.comment = newComment;
					this.readonly = true;
					this.toastrService.success(null, response.message);
				}
			},
			(error) => {
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}

	deleteComment(comment: Comment) {
		this.api.comment.delete(this.dashboardKey, this.idList, this.idTask, comment.id).subscribe(
			(response: ApiResponse) => {
				if (response.statusCode == 200) {
					this.toastrService.success(null, response.message, { icon: 'trash-2-outline' });
					this.onDeleteComment.emit(true);
				}
			},
			(error) => {
				console.log(error);
				let errors = error.error as ApiResponse[];
				errors.forEach((error) => {
					this.toastrService.danger(null, error.message);
				});
			}
		);
	}
}
