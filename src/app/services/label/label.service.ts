import { Injectable } from '@angular/core';
import { HttpQueriesService } from '../util/http-queries.service';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root',
})
export class LabelService {
	private readonly apiEndPoint: string = environment.apiEndpoint;

	constructor(private http: HttpQueriesService) { }

	create(dashboardKey: string, name: string, color: string) {
		return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/labels/`, { name: name, color: '#' + color });
	}

	update(dashboardKey: string, idLabel: number, name: string, color: string) {
		return this.http.putQuery(this.apiEndPoint, `${dashboardKey}/labels/${idLabel}`, { name: name, color: '#' + color });
	}

	delete(dashboardKey: string, idLabel: number) {
		return this.http.deleteQuery(this.apiEndPoint, `${dashboardKey}/labels/${idLabel}`, null);
	}

	getAll(dashboardKey: string) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/labels`, null);
	}

	get(dashboardKey: string, idLabel: number) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/labels/${idLabel}`, null);
	}
}
