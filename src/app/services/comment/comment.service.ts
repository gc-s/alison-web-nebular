import { Injectable } from '@angular/core';
import { HttpQueriesService } from '../util/http-queries.service';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root',
})
export class CommentService {
	private readonly apiEndPoint: string = environment.apiEndpoint;

	constructor(private http: HttpQueriesService) { }

	create(dashboardKey: string, idList: number, idTask: number, comment: string) {
		return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/comment/${idList}/${idTask}`, { comment: comment });
	}

	update(dashboardKey: string, idList: number, idTask: number, idComment: number, comment: string) {
		return this.http.putQuery(this.apiEndPoint, `${dashboardKey}/comment/${idList}/${idTask}/${idComment}`, { comment: comment });
	}

	delete(dashboardKey: string, idList: number, idTask: number, idComment: number) {
		return this.http.deleteQuery(this.apiEndPoint, `${dashboardKey}/comment/${idList}/${idTask}/${idComment}`, null);
	}

	getAll(dashboardKey: string, idList: number, idTask: number) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/comment/${idList}/${idTask}`, null);
	}

	get(dashboardKey: string, idList: number, idTask: number, idComment: number) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/comment/${idList}/${idTask}/${idComment}`, null);
	}
}
