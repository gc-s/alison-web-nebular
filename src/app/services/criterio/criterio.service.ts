import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpQueriesService } from '../util/http-queries.service';

@Injectable({
  providedIn: 'root'
})
export class CriterioService {
  private readonly apiEndPoint: string = environment.apiEndpoint;

  constructor(private http: HttpQueriesService) { }

  create(dashboardKey: string, idList: number, idTask: number, idChecklist: number, indicator: string) {
    return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/criterio/${idList}/${idTask}/${idChecklist}`, { indicator: indicator });
  }

  update(dashboardKey: string, idList: number, idTask: number, idChecklist: number, idCriterio: number, indicator: string) {
    return this.http.putQuery(this.apiEndPoint, `${dashboardKey}/criterio/${idList}/${idTask}/${idChecklist}/${idCriterio}`, { indicator: indicator });
  }

  check(dashboardKey: string, idList: number, idTask: number, idChecklist: number, idCriterio: number) {
    return this.http.putQuery(this.apiEndPoint, `${dashboardKey}/criterio/${idList}/${idTask}/${idChecklist}/${idCriterio}/check`, null);
  }

}