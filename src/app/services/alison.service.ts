import { Injectable, Injector } from '@angular/core';
import { DashboardService } from './dashboard/dashboard.service';
import { MemberService } from './member/member.service';
import { ListService } from './list/list.service';
import { LabelService } from './label/label.service';
import { AssignmentsService } from './assignments/assignments.service';
import { TaskService } from './task/task.service';
import { CommentService } from './comment/comment.service';
import { AuthService } from './auth/auth.service';
import { ChecklistService } from "./checklist/checklist.service";
import { CriterioService } from "./criterio/criterio.service";

@Injectable({
  providedIn: 'root'
})
export class AlisonService {

  private _authService: AuthService;
  private _dashboardService: DashboardService;
  private _memberService: MemberService;
  private _listService: ListService;
  private _labelService: LabelService;
  private _assignmentsService: AssignmentsService;
  private _taskService: TaskService;
  private _commentService: CommentService;
  private _checklistService: ChecklistService;
  private _criterioService: CriterioService;

  public get auth(): AuthService {
    if (!this._authService)
      this._authService = this.injector.get(AuthService);
    return this._authService;
  }

  public get dashboard(): DashboardService {
    if (!this._dashboardService)
      this._dashboardService = this.injector.get(DashboardService);
    return this._dashboardService;
  }

  public get members(): MemberService {
    if (!this._memberService)
      this._memberService = this.injector.get(MemberService);
    return this._memberService;
  }

  public get list(): ListService {
    if (!this._listService)
      this._listService = this.injector.get(ListService);
    return this._listService;
  }

  public get label(): LabelService {
    if (!this._labelService)
      this._labelService = this.injector.get(LabelService);
    return this._labelService;
  }

  public get assignments(): AssignmentsService {
    if (!this._assignmentsService)
      this._assignmentsService = this.injector.get(AssignmentsService);
    return this._assignmentsService;
  }

  public get task(): TaskService {
    if (!this._taskService)
      this._taskService = this.injector.get(TaskService);
    return this._taskService;
  }

  public get comment(): CommentService {
    if (!this._commentService)
      this._commentService = this.injector.get(CommentService);
    return this._commentService;
  }

  public get checklist(): ChecklistService {
    if (!this._checklistService)
      this._checklistService = this.injector.get(ChecklistService);
    return this._checklistService;
  }

  public get criterio(): CriterioService {
    if (!this._criterioService)
      this._criterioService = this.injector.get(CriterioService);
    return this._criterioService;
  }

  constructor(private injector: Injector) { }

}
