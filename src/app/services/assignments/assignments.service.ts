import { Injectable } from '@angular/core';
import { HttpQueriesService } from '../util/http-queries.service';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root',
})
export class AssignmentsService {
	private readonly apiEndPoint: string = environment.apiEndpoint;

	constructor(private http: HttpQueriesService) {}

	assignLabel(clave: string, idTask: number, idLabel: number) {
		return this.http.postQuery(this.apiEndPoint, `${clave}/assign`, { idTask, idLabel });
	}

	unassignLabel(clave: string, idTask: number, idLabel: number) {
		return this.http.postQuery(this.apiEndPoint, `${clave}/unassign`, { idTask, idLabel });
	}

	assignMemberToTasks(clave: string, idTask: number, idUser: number) {
		return this.http.postQuery(this.apiEndPoint, `${clave}/assign/member`, { idTask: idTask, idUser: idUser });
	}

	unassignMemberToTasks(clave: string, idTask: number, idUser: number) {
		return this.http.postQuery(this.apiEndPoint, `${clave}/unassign/member`, { idTask: idTask, idUser: idUser });
	}
}
