import { Injectable } from '@angular/core';
import { HttpQueriesService } from '../util/http-queries.service';
import { environment } from '../../../environments/environment';
import { Rol } from 'src/models/rol';

@Injectable({
	providedIn: 'root',
})
export class MemberService {
	private readonly apiEndPoint: string = environment.apiEndpoint;

	constructor(private http: HttpQueriesService) {}

	joinForm(idDashboard: number, email: string) {
		return this.http.postQuery(this.apiEndPoint, 'join/', { idDashboard: idDashboard, email: email });
	}

	joinLink(code: string) {
		return this.http.getQuery(this.apiEndPoint, `join/${code}`, null);
	}

	searchMember(email: string) {
		return this.http.postQuery(this.apiEndPoint, `s/m`, { email: email });
	}

	getCode(code: string, applications: number, validity: number) {
		return this.http.postQuery(this.apiEndPoint, `code/${code}`, { applications: applications, validity: validity });
	}

	assignRol(code: string, idUser: number, rol: Rol) {
		return this.http.putQuery(this.apiEndPoint, `${code}/set`, { idUser: idUser, rol: rol });
	}

	removeUser(code: string, idUser: number) {
		return this.http.deleteQuery(this.apiEndPoint, `${code}/member/${idUser}`, null);
	}
}
