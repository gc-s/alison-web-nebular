import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpQueriesService } from '../util/http-queries.service';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {
  private readonly apiEndPoint: string = environment.apiEndpoint;

  constructor(private http: HttpQueriesService) { }

  create(dashboardKey: string, idList: number, idTask: number, name: string) {
    return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/check/${idList}/${idTask}`, { name: name });
  }

  getAll(dashboardKey: string, idList: number, idTask: number) {
    return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/check/${idList}/${idTask}`, null);
  }

  get(dashboardKey: string, idList: number, idTask: number, idChecklist: number) {
    return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/check/${idList}/${idTask}/${idChecklist}`, null);
  }
}