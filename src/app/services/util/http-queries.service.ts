import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpQueriesService {

  constructor(private http: HttpClient) { }

  public getQuery( endPoint: string, query: string, params: any ) {
    const url = `${ endPoint }/${ query }`;

    const headers = new HttpHeaders({
      'Authorization':`Bearer ${localStorage.getItem('token')}`
    });

    return this.http.get(url, { headers, params });
  }

  public postQuery( endPoint: string, query: string, params: any ) {
    const url = `${ endPoint }/${ query }`;

    const headers = new HttpHeaders({
      'Authorization':`Bearer ${localStorage.getItem('token')}`
    });

    return this.http.post(url, params, { headers });
  }

  public putQuery( endPoint: string, query: string, params: any ) {
    const url = `${ endPoint }/${ query }`;

    const headers = new HttpHeaders({
      'Authorization':`Bearer ${localStorage.getItem('token')}`
    });

    return this.http.put(url, params, { headers });
  }

  public deleteQuery( endPoint: string, query: string, params: any ) {
    const url = `${ endPoint }/${ query }`;

    const headers = new HttpHeaders({
      'Authorization':`Bearer ${localStorage.getItem('token')}`
    });

    return this.http.delete(url, { headers, params });
  }
}
