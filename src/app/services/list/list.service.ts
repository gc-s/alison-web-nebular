import { Injectable } from '@angular/core';
import { HttpQueriesService } from '../util/http-queries.service';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root',
})
export class ListService {
	private readonly apiEndPoint: string = environment.apiEndpoint;

	constructor(private http: HttpQueriesService) { }

	create(dashboardKey: string, name: string, wip: number) {
		return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/list`, { name: name, wip: wip });
	}

	update(dashboardKey: string, idList: number, name: string, wip: number) {
		return this.http.putQuery(this.apiEndPoint, `${dashboardKey}/list/${idList}`, { name: name, wip: wip });
	}

	delete(dashboardKey: string, idList: number) {
		return this.http.deleteQuery(this.apiEndPoint, `${dashboardKey}/list/${idList}`, null);
	}

	getAll(dashboardKey: string) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/list`, null);
	}

	getArchive(dashboardKey: string) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/list/archive`, null);
	}

	get(dashboardKey: string, idList: number) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/list/${idList}`, null);
	}
}
