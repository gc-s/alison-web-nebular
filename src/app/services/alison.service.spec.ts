import { TestBed } from '@angular/core/testing';

import { AlisonService } from './alison.service';

describe('AlisonService', () => {
  let service: AlisonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlisonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
