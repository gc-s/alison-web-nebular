export class UserAuth {
    id: number = 0;
    name: string = "";
    user: string = "";
    email: string = "";
    accessToken: string = "";
    isAuthenticated: boolean = false;
}