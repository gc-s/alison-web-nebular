import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "../../../environments/environment";  
import { Router } from '@angular/router';
import { UserAuth } from "./user-auth";
import { SignupModel } from "../../models/auth/signup.model";
import { LoginModel } from "../../models/auth/login.model";
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly apiEndPoint: string = environment.apiEndpoint;

  userAuth: UserAuth = new UserAuth();

  constructor( private http: HttpClient, private router: Router ) {}

  signup(user: SignupModel) {
    const url = this.apiEndPoint+'/auth/signup';
    return this.http.post<any>(url, user);
  }

  login(user: LoginModel): Observable<UserAuth>  {
    this.reset();

    const url = this.apiEndPoint+'/auth/login';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post<UserAuth>(url, user, httpOptions).pipe(
      tap( response => {//Array.forEach()
        Object.assign(this.userAuth, {
          id: response.id,
          name: response.name,
          email: response.email,
          user: response.user,
          accessToken: response.accessToken,
          isAuthenticated: true
        });
        localStorage.setItem("token", this.userAuth.accessToken);
      })
    );
  }

  update(name: string, password: string) {
    const headers = new HttpHeaders({
      'Authorization':`Bearer ${localStorage.getItem('token')}`
    });

    const url = this.apiEndPoint+'/auth/update';

    return this.http.put<any>(url, { name: name, password: password }, { headers });
  }

  getUserData() {
    const headers = new HttpHeaders({
      'Authorization':`Bearer ${localStorage.getItem('token')}`
    });

    const url = this.apiEndPoint+'/auth/userdata';

    return this.http.get<UserAuth>(url, { headers }).pipe(
      tap( response => {//Array.forEach()
        Object.assign(this.userAuth, {
          id: response.id,
          name: response.name,
          email: response.email,
          user: response.user,
          accessToken: localStorage.getItem('token'),
          isAuthenticated: true
        });
        localStorage.setItem("token", this.userAuth.accessToken);
      })
    );
  }

  logout(): void {
    this.reset();
    this.router.navigate(['login']);
  }

  getApiStatus() {
    const url = this.apiEndPoint;
    return this.http.get(url);
  }

  reset(): void {
    this.userAuth.id = 0;
    this.userAuth.user = "";
    this.userAuth.name = "";
    this.userAuth.email = "";
    this.userAuth.accessToken = "";
    this.userAuth.isAuthenticated = false;
	
    localStorage.removeItem("token");
  }  

}
