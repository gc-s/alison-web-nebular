import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlisonService } from '../alison.service';

@Injectable({
	providedIn: 'root',
})
export class AuthGuard implements CanActivate {
	constructor(private alisonService: AlisonService, private router: Router) {}

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		//let claimName: string = next.data["claimName"];

		if (localStorage.getItem('token')) {
			if (this.alisonService.auth.userAuth.name === '') {
				this.alisonService.auth.getUserData().subscribe(
					(resp) => {
						// console.log(this.alisonService.auth.userAuth);
					},
					(error) => {
						error.error.forEach((error) => {
							console.log(error);
							this.alisonService.auth.reset();
							this.router.navigate(['login']);
						});
					}
				);
			}
			return true;
		} else {
			//this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
			this.router.navigate(['login']);
			return false;
		}
	}
}
