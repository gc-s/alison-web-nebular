import { Injectable } from '@angular/core';
import { HttpQueriesService } from '../util/http-queries.service';
import { environment } from '../../../environments/environment';

@Injectable({
	providedIn: 'root',
})
export class DashboardService {
	private readonly apiEndPoint: string = environment.apiEndpoint;

	constructor(private http: HttpQueriesService) { }

	//add file
	create(name: string, bg: string) {
		return this.http.postQuery(this.apiEndPoint, 'd/', { name: name, background: '#' + bg });
	}

	//add file
	update(idDashboard: number, name: string, bg: string) {
		return this.http.putQuery(this.apiEndPoint, 'd/update/' + idDashboard, { name: name, background: '#' + bg });
	}

	getAll() {
		return this.http.getQuery(this.apiEndPoint, 'd/', null);
	}

	get(dashboardKey: string) {
		return this.http.getQuery(this.apiEndPoint, 'd/' + dashboardKey, null);
	}

	getArchive() {
		return this.http.getQuery(this.apiEndPoint, 'd/archive', null);
	}

	delete(idDashboard: number) {
		return this.http.deleteQuery(this.apiEndPoint, 'd/delete/' + idDashboard, null);
	}
}
