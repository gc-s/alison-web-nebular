import { Injectable } from '@angular/core';
import { HttpQueriesService } from '../util/http-queries.service';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root',
})
export class TaskService {
	private readonly apiEndPoint: string = environment.apiEndpoint;

	constructor(private http: HttpQueriesService) { }

	create(dashboardKey: string, idList: number, title: string, description: string) {
		return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}`, {
			title: title,
			description: description,
		});
	}

	update(
		dashboardKey: string,
		idList: number,
		idTask: number,
		title: string,
		description: string,
		startDate: string,
		deadLine: string,
		endDate: string,
		estimatedSize: number,
		realSize: number
	) {
		return this.http.putQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}/${idTask}`, {
			title: title,
			description: description,
			startDate: startDate,
			deadLine: deadLine,
			endDate: endDate,
			estimatedSize: estimatedSize,
			realSize: realSize,
		});
	}

	delete(dashboardKey: string, idList: number, idTask: number) {
		return this.http.deleteQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}/${idTask}`, null);
	}

	getAll(dashboardKey: string, idList: number) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}`, null);
	}

	get(dashboardKey: string, idList: number, idTask: number) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}/${idTask}`, null);
	}

	getArchive(dashboardKey: string, idList: number) {
		return this.http.getQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}/archive`, null);
	}

	finished(dashboardKey: string, idList: number, idTask: number) {
		return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}/finished/${idTask}`, null);
	}

	unfinished(dashboardKey: string, idList: number, idTask: number) {
		return this.http.postQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}/unfinished/${idTask}`, null);
	}

	moveTask(dashboardKey: string, idList: number, idTask: number, position: number) {
		return this.http.putQuery(this.apiEndPoint, `${dashboardKey}/task/${idList}/move/${idTask}/${position}`, null);
	}
}
