export class LabelModel {
	id: number;
	name: string;
	color: string;
	check?: boolean;
}
