import { TaskModel } from '../task/task.model';

export class ListModel {
	id: number;
	name: string;
	wip: number;
	position: number;
	archive: boolean;
	tasks: TaskModel[];

	constructor(name: string, wip: number) {
		this.name = name;
		this.wip = wip;
	}
}
