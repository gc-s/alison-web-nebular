import { MemberModel } from '../members/member.model';

export class DashboardModel {
	id: number;
	name: string;
	key: string;
	background: string;
	url: string;
	owner: boolean;
	rol: number;
	members: MemberModel[];

	constructor(name: string, background: string) {
		this.name = name;
		this.background = background;
	}
}
