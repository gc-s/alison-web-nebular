export class User {
    id: number = 0;
    name: string = "";
    email: string = "";
    user: string = "";
    password: string = "";

    constructor(user: string, password: string) {
        this.user = user;
        this.password = password;
    }

}