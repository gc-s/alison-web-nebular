export class CommentDto {
	id: number;
	idUser: number;
	idTask: number;
	comment: string;
	date: Date;
}
