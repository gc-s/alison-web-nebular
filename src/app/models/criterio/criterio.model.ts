export class CriterioModel {
    id: number;
    indicator: string;
    status: boolean;
}
