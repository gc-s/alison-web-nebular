import { ChecklistModel } from "../checklist/checklist.model";

export class TaskModel {
	id: number;
	idList: number;
	position: number;
	title: string;
	description: string;
	startDate: Date;
	deadLine: Date;
	creationDate: Date;
	endDate: Date;
	estimatedSize: number;
	realSize: number;
	archive: boolean;
	members: any[];
	labels: any[];
	checklists: ChecklistModel[];

	constructor(title: string, description: string) { }
}
