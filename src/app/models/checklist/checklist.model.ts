import { CriterioModel } from "../criterio/criterio.model";

export class ChecklistModel {
    id: number;
    name: string;
    criterios: CriterioModel[];
}
