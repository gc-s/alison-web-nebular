import { Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { HomeComponent } from './pages/home/home.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LabelsComponent } from './pages/labels/labels.component';
import { AuthGuard } from './services/auth/auth.guard';

export const ROUTES: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
	{ path: 'dashboard/:code', component: DashboardComponent, canActivate: [AuthGuard] },
	{ path: 'join/:code', component: DashboardComponent, canActivate: [AuthGuard] },
	{ path: 'dashboard/:code/labels', component: LabelsComponent, canActivate: [AuthGuard] },
	{ path: '', pathMatch: 'full', redirectTo: 'home' },
	{ path: '**', pathMatch: 'full', redirectTo: 'home' },
];
