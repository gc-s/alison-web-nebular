import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlisonService } from "src/app/services/alison.service";
import { Router } from '@angular/router';
import { UserAuth } from 'src/app/services/auth/user-auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  loginFG: FormGroup;
  errors: string[] = [];
  messages: string[] = [];
  submitted: boolean = false;

  constructor(private fb: FormBuilder, private api: AlisonService, private router: Router) { 
    this.buildLoginForm();
  }

  ngOnInit(): void {
  }

  setSubmit(){
    this.submitted = true;
    this.errors = [];
    this.messages = [];
  }

  login() {
    this.setSubmit();
    if(this.loginFG.valid) {
      this.api.auth.login({ user: this.inputUser.value, password: this.inputPassword.value }).subscribe(resp => {
        this.api.auth.userAuth = resp;
        this.router.navigate(['home']);
      }, (error =>{
        error.error.forEach(error => {
          console.log(error)
          this.errors.push(error.message)
          this.api.auth.userAuth = new UserAuth();
        });
      }));

    } else {
      return Object.values( this.loginFG.controls ).forEach( control => {
        control.markAsTouched();
      })
    }
  }

  buildLoginForm(){
    this.loginFG = this.fb.group({
      user: ['', [ Validators.required, Validators.minLength(4) ] ],
      password: ['', [ Validators.required, Validators.minLength(8) ] ]
    })
  }

  get inputUser() {
    return this.loginFG.get('user');
  }

  get inputPassword() {
    return this.loginFG.get('password');
  }


}
