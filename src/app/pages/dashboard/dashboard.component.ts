import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlisonService } from 'src/app/services/alison.service';
import { List } from 'src/models/list';
import { DashboardModel } from 'src/app/models/dashboard/dashboard.model';
import { NbMenuItem, NbMenuService, NbDialogService, NbToastrService } from '@nebular/theme';
import { DialogListComponent } from '../../components/modals/dialog-list/dialog-list.component';
import { Task } from 'src/models/task';
import { Member } from 'src/models/member';
import { DialogMembersComponent } from '../../components/modals/dialog-members/dialog-members.component';
import { filter, map } from 'rxjs/operators';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styles: [],
})
export class DashboardComponent implements OnInit, OnDestroy {
	lists: List[] = [];

	members: Member[] = [];
	selectedTask: Task;
	idList: number;
	color: string;
	dashboardKey: string;
	name: string;
	menuSide: NbMenuItem[] = [
		{ title: 'Etiquetas', icon: 'pricetags-outline', link: 'labels' },
		{ title: 'Miembros', icon: 'people-outline' },
	];

	private selectMenu: any;

	constructor(
		private elementRef: ElementRef,
		private route: ActivatedRoute,
		private router: Router,
		private api: AlisonService,
		private dialogService: NbDialogService,
		private nbMenuService: NbMenuService
	) {
		this.route.params.subscribe((params) => {
			if (this.router.url.split('/')[1] === 'join') {
				this.api.members.joinLink(params['code']).subscribe((response: any) => {
					console.log(response);
					router.navigate(['dashboard', response.dashboardKey]);
				});
			} else {
				this.dashboardKey = params['code'];
				this.getDashboard(this.dashboardKey);
			}
		});
	}

	ngOnInit(): void {
		this.selectMenu = this.nbMenuService
			.onItemClick()
			.pipe(
				filter(({ tag }) => tag === 'sidebar-menu'),
				map(({ item: { icon } }) => icon)
			)
			.subscribe((icon) => {
				console.log(icon);
				switch (icon) {
					case 'people-outline':
						this.openMembersDialog();
						break;
				}
			});
	}

	ngOnDestroy(): void {
		this.elementRef.nativeElement.ownerDocument.getElementsByClassName('columns')[0].style.backgroundColor = '#edf1f7';
		this.selectMenu.unsubscribe();
	}

	getDashboard(code: string) {
		this.api.dashboard.get(code).subscribe((response: DashboardModel) => {
			this.color = response.background;
			this.elementRef.nativeElement.ownerDocument.getElementsByClassName(
				'columns'
			)[0].style.backgroundColor = this.color;
			this.getAllList();
		});
	}

	getAllList() {
		this.api.list.getAll(this.dashboardKey).subscribe((response: any) => {
			//console.log(response);
			response.map((list) => {
				/*list.tareas.map((task) => {
					if (!task.miembros[0].idUser) task.miembros = [];
					task.miembros = task.miembros.map((member) => {
						member.idUser.rol = member.rol;
						delete member.rol;
						delete member.idUser.password;
						return member.idUser;
					});
					return task;
				});*/
				return list;
			});
			this.lists = response as List[];
		});
	}

	openListDialog() {
		this.dialogService
			.open(DialogListComponent, {
				context: {
					dashboardKey: this.dashboardKey,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.getAllList();
			});
	}

	openMembersDialog() {
		this.dialogService
			.open(DialogMembersComponent, {
				context: {
					dashboardKey: this.dashboardKey,
					members: this.members,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.getAllList();
			});
	}

	refresh() {
		this.getAllList();
	}
}
