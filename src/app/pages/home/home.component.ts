import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbMenuItem } from '@nebular/theme';
import { AlisonService } from 'src/app/services/alison.service';
import { DialogBoardComponent } from 'src/app/components/modals/dialog-board/dialog-board.component';
import { UserAuth } from 'src/app/services/auth/user-auth';
import { DashboardModel } from 'src/app/models/dashboard/dashboard.model';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styles: [],
})
export class HomeComponent implements OnInit {
	boardsOwn: DashboardModel[] = [];
	boardsShare: DashboardModel[] = [];

	sidebarMenu: NbMenuItem[] = [];
	userAuth: UserAuth = null;

	constructor(private api: AlisonService, private dialogService: NbDialogService) {
		this.userAuth = this.api.auth.userAuth;
	}

	ngOnInit(): void {
		this.getAllBoards();
	}

	getAllBoards() {
		this.api.dashboard.getAll().subscribe(
			(response: DashboardModel[]) => {
				//console.log(response);
				this.boardsOwn = response.filter(function (board) {
					return board.owner === true;
				});
				this.boardsShare = response.filter(function (board) {
					return board.owner === false;
				});
			},
			(error) => {
				console.log(error);
			}
		);
	}

	refresh() {
		this.getAllBoards();
	}

	openNewBoardDialog() {
		this.dialogService.open(DialogBoardComponent).onClose.subscribe((result) => {
			if (result) this.getAllBoards();
		});
	}
}
