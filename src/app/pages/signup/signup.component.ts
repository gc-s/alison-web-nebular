import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlisonService } from 'src/app/services/alison.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles: [
  ]
})
export class SignupComponent implements OnInit {

  signupFG: FormGroup;
  errors: string[] = [];
  messages: string[] = [];
  submitted: boolean = false;
  acceptedTerms: boolean = false;

  constructor(private fb: FormBuilder, private api: AlisonService, private router: Router) {
    this.buildSignupForm();
  }

  ngOnInit(): void {
  }

  setSubmit(){
    this.submitted = true;
    this.errors = [];
    this.messages = [];
  }

  signup(){
    this.setSubmit();
    if (!this.acceptedTerms)
      this.errors.push("Para registrarte debes estar de acuerdo con los términos y condiciones");
    else if(this.signupFG.valid){
        this.api.auth.signup({
          name: this.inputName.value,
          email: this.inputEmail.value, 
          user: this.inputUsername.value,
          password: this.inputPassword.value
        }).subscribe((response: any) => {
          if (response.statusCode == 201){
            this.messages.push("Te has registrado correctamente!")
          }
        }, (error =>{
          error.error.forEach(error => {
            console.log(error)
            this.errors.push(error.message)
          });
        }));
      }else{
        return Object.values( this.signupFG.controls ).forEach( control => {
          control.markAsTouched();
        })
      }
  }

  setTerms($event){
    this.acceptedTerms = $event;
  }

  buildSignupForm(){
    this.signupFG = this.fb.group({
      name: ['', [ Validators.required, Validators.minLength(4) ] ],
      email: ['', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$') ] ],
      username: ['', [ Validators.required, Validators.minLength(4) ] ],
      password: ['', [ Validators.required, Validators.minLength(8) ] ],
    })
  }

  get inputName(){
    return this.signupFG.get('name');
  }

  get inputEmail(){
    return this.signupFG.get('email');
  }

  get inputUsername() {
    return this.signupFG.get('username');
  }

  get inputPassword() {
    return this.signupFG.get('password');
  }

}