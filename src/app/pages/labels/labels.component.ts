import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlisonService } from 'src/app/services/alison.service';
import { NbToastrService, NbDialogService, NbMenuItem } from '@nebular/theme';
import { DialogLabelComponent } from '../../components/modals/dialog-label/dialog-label.component';
import { DialogConfirmComponent } from '../../components/modals/dialog-confirm/dialog-confirm.component';
import { LabelModel } from '../../models/label/label.model';

@Component({
	selector: 'app-labels',
	templateUrl: './labels.component.html',
	styles: [],
})
export class LabelsComponent implements OnInit {
	menuSide: NbMenuItem[] = [{ title: 'Tablero', icon: 'clipboard-outline', link: '..' }];
	labels: LabelModel[] = [];
	dashboardKey: string;

	constructor(
		private route: ActivatedRoute,
		private api: AlisonService,
		private toastrService: NbToastrService,
		private dialogService: NbDialogService
	) {
		this.route.params.subscribe((params) => {
			this.dashboardKey = params['code'];
			this.getLabels(this.dashboardKey);
		});
	}

	ngOnInit(): void {}

	getLabels(code: string) {
		this.api.label.getAll(code).subscribe((response: any) => {
			this.labels = response;
		});
	}

	openLableDialog(editMode: boolean, label: LabelModel) {
		this.dialogService
			.open(DialogLabelComponent, {
				context: {
					editMode: editMode,
					label: label,
					dashboardKey: this.dashboardKey,
				},
			})
			.onClose.subscribe((result) => {
				if (result) this.getLabels(this.dashboardKey);
			});
	}

	openDeleteLabelDialog(label: LabelModel) {
		this.dialogService
			.open(DialogConfirmComponent, {
				context: {
					title: '¿Estás seguro?',
					content: 'Se eliminará la etiqueta y no podrás recuperarla.',
					icon: 'alert-triangle-outline',
					showCancelButton: true,
					confirmButtonText: 'Eliminar etiqueta',
				},
			})
			.onClose.subscribe((result) => {
				if (result)
					this.api.label.delete(this.dashboardKey, label.id).subscribe(
						(response: any) => {
							if (response.statusCode == 200) {
								this.toastrService.success(null, `Se eliminó la etiqueta`, { icon: 'trash-2-outline' });
								this.getLabels(this.dashboardKey);
							}
						},
						(error) => {
							this.toastrService.danger(null, `No se pudo eliminar la etiqueta`);
							console.log(error);
						}
					);
			});
	}
}
