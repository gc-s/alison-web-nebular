import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth/auth.guard';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
	NbThemeModule,
	NbLayoutModule,
	NbSidebarModule,
	NbActionsModule,
	NbCardModule,
	NbIconModule,
	NbButtonModule,
	NbAlertModule,
	NbInputModule,
	NbCheckboxModule,
	NbMenuModule,
	NbDialogModule,
	NbFormFieldModule,
	NbAccordionModule,
	NbToastrModule,
	NbPopoverModule,
	NbListModule,
	NbTabsetModule,
	NbBadgeModule,
	NbContextMenuModule,
	NbDatepickerModule,
	NbToggleModule,
	NbUserModule,
	NbProgressBarModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { HomeComponent } from './pages/home/home.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LabelsComponent } from './pages/labels/labels.component';
import { ROUTES } from './app.routes';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';
import { DialogBoardComponent } from './components/modals/dialog-board/dialog-board.component';
import { DialogShareBoardComponent } from './components/modals/dialog-share-board/dialog-share-board.component';
import { DialogConfirmComponent } from './components/modals/dialog-confirm/dialog-confirm.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { DialogListComponent } from './components/modals/dialog-list/dialog-list.component';
import { DialogTaskComponent } from './components/modals/dialog-task/dialog-task.component';
import { DialogLabelComponent } from './components/modals/dialog-label/dialog-label.component';
import { DialogLabelTaskComponent } from './components/modals/dialog-label-task/dialog-label-task.component';
import { DialogMembersComponent } from './components/modals/dialog-members/dialog-members.component';
import { CommentComponent } from './components/comment/comment.component';
import { TruncatePipe } from './pipes/truncate.pipe';
import { ListBoardsComponent } from './components/list-boards/list-boards.component';
import { ListComponent } from './components/list/list.component';
import { ListTaskComponent } from './components/list-task/list-task.component';
import { DialogChecklistComponent } from './components/modals/dialog-checklist/dialog-checklist.component';
import { ChecklistComponent } from './components/checklist/checklist.component';
import { DialogCriterioComponent } from './components/modals/dialog-criterio/dialog-criterio.component';
@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		SidebarComponent,
		LoginComponent,
		SignupComponent,
		DialogBoardComponent,
		DialogShareBoardComponent,
		DialogConfirmComponent,
		DashboardComponent,
		HeaderComponent,
		FooterComponent,
		DialogListComponent,
		DialogTaskComponent,
		DialogLabelComponent,
		LabelsComponent,
		DialogLabelTaskComponent,
		DialogMembersComponent,
		CommentComponent,
		TruncatePipe,
		ListBoardsComponent,
		ListTaskComponent,
		ListComponent,
		DialogChecklistComponent,
		ChecklistComponent,
		DialogCriterioComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		NbThemeModule.forRoot({ name: 'corporate' }),
		NbLayoutModule,
		NbEvaIconsModule,
		NbActionsModule,
		NbCardModule,
		NbIconModule,
		NbButtonModule,
		NbInputModule,
		NbCheckboxModule,
		NbAlertModule,
		NbFormFieldModule,
		NbAccordionModule,
		NbPopoverModule,
		NbListModule,
		NbContextMenuModule,
		NbBadgeModule,
		NbToggleModule,
		NbUserModule,
		NbPopoverModule,
		NbTabsetModule,
		NbProgressBarModule,
		NbDatepickerModule.forRoot(),
		NbToastrModule.forRoot(),
		NbMenuModule.forRoot(),
		NbDialogModule.forRoot(),
		NbSidebarModule.forRoot(),
		DragDropModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forRoot(ROUTES, { useHash: false, relativeLinkResolution: 'legacy' }),
	],
	providers: [HttpClientModule, AuthGuard],
	bootstrap: [AppComponent],
})
export class AppModule { }
